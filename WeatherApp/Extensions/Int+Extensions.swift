//
//  Int+Extensions.swift
//  WeatherApp
//
//  Created by A-10474 on 29/01/22.
//

import Foundation

enum Formatter: String {
    case ddmmyyyy = "dd MM yyyy"
    case ddEEEyyyy = "dd EEE yyyy"
    case ddMMMyyyy = "dd MMM yyyy"
    case HHmma = "HH:mm a"
    case hhmma = "hh:mm a"
}

extension Int {
    
    func convertToDate(with formatter: Formatter = .ddmmyyyy)-> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatter.rawValue
        let timeInterval = TimeInterval(self)
        let date = Date(timeIntervalSince1970: timeInterval)
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
}
