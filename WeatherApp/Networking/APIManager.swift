//
//  APIManager.swift
//  WeatherApp
//
//  Created by A-10474 on 23/01/22.
//

import Foundation
import Combine

class APIManager {
    
    static let shared = APIManager()
    
    func request<T: Decodable>(_ request: URLRequestBuilder, responseType: T.Type)-> AnyPublisher<T, Error> {
        return URLSession.shared.dataTaskPublisher(for: request.baseRequest)
            .receive(on: RunLoop.main)
            .map(\.data)
            .decode(type: responseType.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}
