//
//  APIEnvironment.swift
//  WeatherApp
//
//  Created by A-10474 on 23/01/22.
//

import Foundation

enum APIEnvironment {
    
    static let current: APIEnvironment = .development
    
    case development
    case production
    
    var baseUrl: String {
        switch self {
        case .development: return "https://api.openweathermap.org/data/2.5/onecall"
        case .production: return "productionUrl"
        }
    }
}
