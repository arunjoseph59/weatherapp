//
//  URLRequestBuilder.swift
//  WeatherApp
//
//  Created by A-10474 on 23/01/22.
//

import Foundation
import Combine

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
}

protocol URLRequestBuilder {
    var baseUrl: URL { get }
    var endPoint: String { get }
    var headers: [String: String]? { get }
    var requestURL: URL { get }
    var method: HTTPMethod { get }
    var parameters: Data? { get }
    func asURLRequest() throws -> URLRequest
}

extension URLRequestBuilder {
    
    var baseUrl: URL {
        return URL(string: APIEnvironment.current.baseUrl)!
    }
    
    var requestURL: URL {
        guard let urlString = (baseUrl.absoluteString + endPoint).removingPercentEncoding, let url = URL(string: urlString) else { return requestURL}
        return url
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var baseRequest: URLRequest {
        var request = URLRequest(url: requestURL)
        request.url = requestURL
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = headers
        request.httpBody = parameters
        return request
    }
}
