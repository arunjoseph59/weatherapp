//
//  CityViewModel.swift
//  WeatherApp
//
//  Created by A-10474 on 30/01/22.
//

import Foundation

class CityViewModel {
    
    private var cities: [City] = [
        City(cityName: "New Delhi, India", latitude: "28.6139", longitude: "77.2090"),
        City(cityName: "Aberdeen, Scotland", latitude: "57.9", longitude: "2.9"),
        City(cityName: "Adelaide, Australia", latitude: "34.55", longitude: "138.36"),
        City(cityName: "Algiers, Algeria", latitude: "36.50", longitude: "3.0"),
        City(cityName: "Amsterdam, Netherlands", latitude: "52.22", longitude: "4.53"),
        City(cityName: "Ankara, Turkey", latitude: "39.55", longitude: "32.55"),
        City(cityName: "Asuncin, Paraguay", latitude: "25.15", longitude: "57.40"),
        City(cityName: "Athens, Greece", latitude: "37.58", longitude: "23.43"),
        City(cityName: "Auckland, New Zealand", latitude: "36.52", longitude: "174.45"),
        City(cityName: "Bangkok, Thailand", latitude: "13.45", longitude: "100.30"),
    ]
    
    func getCities()-> [City] {
        return cities
    }
}
