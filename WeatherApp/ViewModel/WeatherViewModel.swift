//
//  WeatherViewModel.swift
//  WeatherApp
//
//  Created by A-10474 on 25/01/22.
//

import Foundation
import SwiftUI
import Combine

enum States {
    case idle
    case loading
    case failed(Error)
    case loaded(WeatherDataModel)
}

class WeatherViewModel: ObservableObject, Retryable {
    
    ///Retryable
    var retry: Bool = false {
        didSet {
            self.state = .idle
        }
    }
    
    @Published var city: City {
        didSet {
            self.state = .idle
        }
    }
    @Published private(set) var state: States
    private var cancellable: AnyCancellable?
    private var weatherDataModel = WeatherDataModel()
    
    init(city: City, state: States) {
        self.city = city
        self.state = state
    }
    
    //MARK: Fetch weather API call
    func fetchWeather() {
        self.state = .loading
        let weatherRequest = WeatherRequest(lat: city.latitude, long: city.longitude, exclude: "minutely")
        let request = WeatherRouter.getWeather(params: weatherRequest)
        self.cancellable = APIManager.shared.request(request, responseType: WeatherResponse.self)
            .print("Debigging")
            .sink(receiveCompletion: { [weak self] completion in
                print(completion)
                switch completion {
                case .failure(let error):
                    self?.state = .failed(error)
                case .finished:
                    break
                }
            }, receiveValue: {[weak self] weatherResponse in
                guard let self = self else { return }
                self.weatherDataModel.currentWeather = weatherResponse.current
                self.weatherDataModel.hourlyWeather = weatherResponse.hourly
                self.weatherDataModel.dailyWeather = weatherResponse.daily
                self.state = .loaded(self.weatherDataModel)
            })
    }
    
    //MARK: Convert to Psi and return string
    func convertToPsi(pressure: Double?)-> String {
        guard let pressure = pressure else { return "" }
        let onePsi = 0.0001450377
        let convertedValue = onePsi*pressure
        let twoDecimalVale = Double(round(100*convertedValue)/100)
        return twoDecimalVale.description+"Psi"
    }
}
