//
//  CitySelectionView.swift
//  WeatherApp
//
//  Created by A-10474 on 24/01/22.
//

import SwiftUI

struct CitySelectionView: View {
    
    @Binding var city: City
    let viewModel = CityViewModel()
    
    init(city: Binding<City>) {
        
        self._city = city
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().isTranslucent = true
        UINavigationBar.appearance().tintColor = .clear
        UINavigationBar.appearance().backgroundColor = .clear
        UINavigationBar.appearance().largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.white]
        UITableView.appearance().backgroundColor = .clear
        UITableViewCell.appearance().backgroundColor = .clear
    }
    
    @Environment(\.presentationMode) var presentationMode
    var btnBack : some View { Button(action: {
        self.presentationMode.wrappedValue.dismiss()
            }) {
                HStack {
                Image(systemName: "chevron.left") // set image here
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(.white)
                    Text("Back")
                        .foregroundColor(.white)
                }
            }
        }
    
    var body: some View {
            ZStack {
                Color.black
                    .opacity(0.9)
                    .edgesIgnoringSafeArea(.all)
                List {
                    ForEach(viewModel.getCities(), id: \.self) { city in
                        Text(city.cityName)
                            .font(.system(size: 18.0, weight: .medium))
                            .foregroundColor(.white)
                            .listRowBackground(Color.clear)
                            .onTapGesture {
                                self.city = city
                                self.presentationMode.wrappedValue.dismiss()
                                print("CITY IS \(city)")
                            }
                    }
                }
            }
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(leading: btnBack)
            .navigationTitle(Text("Cities"))
    }
}

struct CitySelectionView_Previews: PreviewProvider {
    static var previews: some View {
        CitySelectionView(city: .constant(City(cityName: "New Delhi, India", latitude: "28.6139", longitude: "77.2090")))
    }
}
