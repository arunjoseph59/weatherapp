//
//  WeatherCardView.swift
//  WeatherApp
//
//  Created by A-10474 on 23/01/22.
//

import Foundation
import SwiftUI

struct MainWeatherCardView: View {
    var temp: Double
    var description: String
    var time: Int
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 10.0, style: .continuous)
                .fill(Color.blue)
                .opacity(0.5)
                .shadow(radius: 10)
                .frame(width: 200.0, height: 250.0, alignment: .center)
            VStack(spacing: 5.0) {
                Text(time.convertToDate(with: .hhmma))
                    .foregroundColor(.white)
                    .font(.system(size: 15.0))
                Image("weather")
                    .resizable()
                    .frame(width: 80.0, height: 80.0, alignment: .center)
                    .aspectRatio(contentMode: .fit)
                Text(temp.description+"F")
                    .foregroundColor(.white)
                    .font(.system(size: 25.0, weight: .semibold))
                Text(description)
                    .foregroundColor(.white)
                    .font(.system(size: 15.0))
            }.padding()
        }
    }
}

struct MainWeatherCardView_PreviewProvider: PreviewProvider {
    static var previews: some View {
        MainWeatherCardView(temp: 1.2, description: "description", time: 100000)
    }
}
