//
//  ContentView.swift
//  WeatherApp
//
//  Created by A-10474 on 23/01/22.
//

import SwiftUI

struct MainWeatherView: View {
    
    @EnvironmentObject var viewModel: WeatherViewModel
    
    var body: some View {
        
        switch viewModel.state {
        
        case .idle:
            Color.clear
                .onAppear(perform: viewModel.fetchWeather)
            
        case .failed(let error):
            ErrorView(errorDescription: error.localizedDescription, retryable: self.viewModel)
            
        case .loaded(let weatherDataModel):
            NavigationView {
                ZStack {
                    Color.black
                        .opacity(0.9)
                        .edgesIgnoringSafeArea(.all)
                    GeometryReader { geo in
                        ScrollView(.vertical) {
                            ZStack {
                                HStack {
                                    Spacer()
                                    NavigationLink(destination: CitySelectionView(city: $viewModel.city)) {
                                        Image(systemName: "magnifyingglass")
                                            .resizable()
                                            .frame(width: 30.0, height: 30.0, alignment: .leading)
                                            .foregroundColor(.white)
                                            .padding(.all, 5)
                                            .padding(.trailing, 10)
                                    }
                                    .navigationBarHidden(true)
                                }
                                Spacer()
                                HStack {
                                    Text("Weather")
                                        .foregroundColor(.white)
                                        .font(.system(size: 25.0, weight: .heavy))
                                }
                            }
                            VStack(alignment: .center, spacing: 40.0) {
                                Spacer()
                                HStack {
                                    Text(viewModel.city.cityName)
                                        .foregroundColor(.white)
                                        .font(.system(size: 16.0, weight: .heavy))
                                    Spacer()
                                    Text(weatherDataModel.currentWeather?.dt.convertToDate(with: .ddMMMyyyy) ?? "")
                                        .foregroundColor(.white)
                                        .font(.system(size: 16.0, weight: .heavy))
                                }.padding(.horizontal)
                                ScrollView( .horizontal) {
                                    HStack {
                                        ForEach(weatherDataModel.hourlyWeather ?? [], id: \.self) { weather in
                                            MainWeatherCardView(temp: weather.temp, description: weather.weather.first?.description ?? "", time: weather.dt)
                                                .padding(.horizontal)
                                        }
                                    }
                                }
                                HStack {
                                    Spacer()
                                    ZStack {
                                        RoundedRectangle(cornerRadius: 25.0, style: .continuous)
                                            .fill(Color.blue)
                                            .opacity(0.5)
                                            .shadow(radius: 10)
                                            .frame(width: geo.size.width - 50.0, height: 150.0, alignment: .center)
                                        HStack {
                                            Spacer()
                                            VStack(spacing: 2) {
                                                Image("pressure")
                                                    .resizable()
                                                    .frame(width: 20.0, height: 20.0, alignment: .center)
                                                Text(viewModel.convertToPsi(pressure: weatherDataModel.currentWeather?.pressure))
                                                    .foregroundColor(.white)
                                                    .font(.system(size: 12.0, weight: .heavy))
                                                Text("Pressure")
                                                    .foregroundColor(.white)
                                                    .font(.system(size: 12.0))
                                            }
                                            Spacer()
                                            
                                            VStack(spacing: 2) {
                                                Image("humidity")
                                                    .resizable()
                                                    .frame(width: 20.0, height: 20.0, alignment: .center)
                                                Text((weatherDataModel.currentWeather?.humidity.description ?? "0") + "%")
                                                    .foregroundColor(.white)
                                                    .font(.system(size: 12.0, weight: .heavy))
                                                Text("Humidity")
                                                    .foregroundColor(.white)
                                                    .font(.system(size: 12.0))
                                            }
                                            Spacer()
                                            
                                            VStack(spacing: 2) {
                                                Image("weather")
                                                    .resizable()
                                                    .frame(width: 20.0, height: 20.0, alignment: .center)
                                                Text((weatherDataModel.currentWeather?.clouds.description ?? "0") + "%")
                                                    .foregroundColor(.white)
                                                    .font(.system(size: 12.0, weight: .heavy))
                                                Text("Clouds")
                                                    .foregroundColor(.white)
                                                    .font(.system(size: 12.0))
                                            }
                                            Spacer()
                                            
                                            VStack(spacing: 2) {
                                                Image("temprature")
                                                    .resizable()
                                                    .frame(width: 20.0, height: 20.0, alignment: .center)
                                                Text((weatherDataModel.currentWeather?.temp.description ?? "0") + "%")
                                                    .foregroundColor(.white)
                                                    .font(.system(size: 12.0, weight: .heavy))
                                                Text("Temprature")
                                                    .foregroundColor(.white)
                                                    .font(.system(size: 12.0))
                                            }
                                            Spacer()
                                        }
                                    }
                                }
                                HStack {
                                    Text("Today")
                                        .foregroundColor(.white)
                                        .font(.system(size: 20.0, weight: .heavy))
                                    Spacer()
                                    Text("Next 7 days")
                                        .foregroundColor(.white)
                                        .font(.system(size: 20.0, weight: .heavy))
                                }
                                .padding(.horizontal)
                                ScrollView(.horizontal) {
                                    HStack {
                                        ForEach(weatherDataModel.dailyWeather ?? [], id: \.self) { dailyWeather in
                                            let index = weatherDataModel.dailyWeather?.firstIndex(of: dailyWeather)
                                            DaysCardView(opacity: index == 0 ? 1: 0.5, time: dailyWeather.dt, temp: dailyWeather.temp.day)
                                                .padding(.horizontal)
                                        }
                                    }
                                }
                                Spacer()
                            }
                        }
                    }
                }
            }
            
        case .loading:
            ProgressView()
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MainWeatherView()
    }
}
