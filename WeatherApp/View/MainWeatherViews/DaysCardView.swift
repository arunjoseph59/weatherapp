//
//  DaysCardView.swift
//  WeatherApp
//
//  Created by A-10474 on 23/01/22.
//

import Foundation
import SwiftUI

struct DaysCardView: View {
    
    var opacity: CGFloat
    var time: Int?
    var temp: Double?
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 10.0, style: .continuous)
                .fill(Color.blue)
                .opacity(0.5)
                .shadow(radius: 10)
                .frame(width: 150.0, height: 180.0, alignment: .center)
            VStack(spacing: 5.0) {
                Text(time?.convertToDate(with: .ddEEEyyyy) ?? "")
                    .foregroundColor(.white)
                    .font(.system(size: 12.0))
                Image("weather")
                    .resizable()
                    .frame(width: 80.0, height: 80.0, alignment: .center)
                    .aspectRatio(contentMode: .fit)
                Text((temp?.description ?? "")+"F")
                    .foregroundColor(.white)
                    .font(.system(size: 15.0, weight: .heavy))
            }.padding()
        }
    }
}

struct DaysCardView_PreviewProvider: PreviewProvider {
    static var previews: some View {
        DaysCardView(opacity: 0.5, time: 1000000, temp: 1.2)
    }
}
