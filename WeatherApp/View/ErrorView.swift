//
//  ErrorView.swift
//  WeatherApp
//
//  Created by A-10474 on 29/01/22.
//

import Foundation
import SwiftUI

protocol Retryable {
    var retry: Bool { get set }
}

struct ErrorView: View {
    
    var errorDescription: String
    @State var retryable: Retryable
    
    var body: some View {
        ZStack {
            Color.black
                .edgesIgnoringSafeArea(.all)
            VStack{
                Image("Error")
                    .resizable()
                    .frame(width: 100.0, height: 100.0, alignment: .center)
                    .aspectRatio(contentMode: .fit)
                Text(errorDescription)
                    .foregroundColor(.white)
                    .font(.system(size: 15.0, weight: .medium))
                Button(action: {
                    self.retryable.retry = true
                }) {
                    Text("Retry")
                        .foregroundColor(.white)
                        .font(.system(size: 15.0, weight: .heavy))
                        .padding(12.0)
                }
            }
        }
    }
}

struct ErrorView_PreviewProvider: PreviewProvider {
    static var previews: some View {
        ErrorView(errorDescription: "error occured", retryable: WeatherViewModel(city: City(cityName: "New Delhi, India", latitude: "28.6139", longitude: "77.2090"), state: .idle))
    }
}
