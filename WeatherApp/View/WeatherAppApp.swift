//
//  WeatherAppApp.swift
//  WeatherApp
//
//  Created by A-10474 on 23/01/22.
//

import SwiftUI

@main
struct WeatherAppApp: App {
    var body: some Scene {
        WindowGroup {
            let viewModel = WeatherViewModel(city: City(cityName: "New Delhi, India", latitude: "28.6139", longitude: "77.2090"), state: .idle)
            MainWeatherView().environmentObject(viewModel)
        }
    }
}
