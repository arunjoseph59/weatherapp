//
//  WeatherRouter.swift
//  WeatherApp
//
//  Created by A-10474 on 25/01/22.
//

import Foundation
import Combine

enum WeatherRouter: URLRequestBuilder {
    
    case getWeather(params: WeatherRequest)

    var endPoint: String {
        switch self {
        case .getWeather(let params):
            return "?lat=\(params.lat)&lon=\(params.long)&exclude=\(params.exclude)&appid=\(Constants.APIContants.apiKey)"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getWeather:
            return .get
        }
    }
    
    var parameters: Data? {
        return nil
    }
    
    func asURLRequest() throws -> URLRequest {
        return baseRequest
    }
}
