//
//  CustomError.swift
//  WeatherApp
//
//  Created by A-10474 on 29/01/22.
//

import Foundation

enum CustomError: Error {
    case somethingWentWrong
}

extension CustomError: LocalizedError {
    public var errorDescription: String? {
        switch self{
        case .somethingWentWrong:
            return "Some thing went wrong"
        }
    }
}
