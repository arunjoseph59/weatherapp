//
//  WeatherRequest.swift
//  WeatherApp
//
//  Created by A-10474 on 29/01/22.
//

import Foundation

struct WeatherRequest: Encodable {
    
    var lat: String
    var long: String
    var exclude: String
}
