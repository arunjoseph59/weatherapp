//
//  WeatherResponse.swift
//  WeatherApp
//
//  Created by A-10474 on 25/01/22.
//

import Foundation

struct WeatherResponse: Decodable {
    let current: CurrentWeather
    let daily: [Daily]
    let hourly: [CurrentWeather]
}

struct CurrentWeather: Decodable, Hashable {
    let weather: [Weather]
    let dt: Int
    let temp: Double
    let humidity: Double
    let pressure: Double
    let clouds: Int
}

struct Weather: Decodable, Hashable {
    let id: Int
    let main: String
    let description: String
}

struct Daily: Decodable, Hashable {
    let dt: Int
    let temp: Temperature
    let pressure: Double
    let humidity: Double
    let weather: [Weather]
}

struct Temperature: Decodable, Hashable {
    let day: Double
    let min: Double
    let max: Double
    let night: Double
    let eve: Double
    let morn: Double
}
