//
//  City.swift
//  WeatherApp
//
//  Created by A-10474 on 24/01/22.
//

import Foundation

struct City: Identifiable, Hashable {
    var id = UUID()
    let cityName: String
    let latitude: String
    let longitude: String
}
