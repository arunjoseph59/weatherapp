//
//  WeatherDataModel.swift
//  WeatherApp
//
//  Created by A-10474 on 30/01/22.
//

import Foundation

struct WeatherDataModel {
    
    var error: String = ""
    var currentWeather: CurrentWeather?
    var dailyWeather: [Daily]?
    var hourlyWeather: [CurrentWeather]?
}
